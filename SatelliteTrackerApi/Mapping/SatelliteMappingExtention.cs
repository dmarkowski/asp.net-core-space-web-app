﻿using SatelliteTrackerApi.Models;
using SatelliteTrackerShared.Models.Dto;

namespace SatelliteTrackerApi.Mapping
{
    public static class SatelliteMappingExtention
    {
        public static SatelliteDto Map(this Satellite satellite) =>
            new SatelliteDto
            {
                Id = satellite.Id,
                Name = satellite.Name,
                InternationalDesignator = satellite.InternationalDesignator,
                Epoch = satellite.Epoch,
                MeanMotion = satellite.MeanMotion,
                Eccentricity = satellite.Eccentricity,
                Inclination = satellite.Inclination,
                RightAscensionOfAscendingNode = satellite.RightAscensionOfAscendingNode,
                ArgOfPericenter = satellite.ArgOfPericenter,
                MeanAnomaly = satellite.MeanAnomaly,
                EphemerisType = satellite.EphemerisType,
                ClassificationType = satellite.ClassificationType,
                ElementNumber = satellite.ElementNumber,
                RevAtEpoch = satellite.RevAtEpoch,
                Bstar = satellite.Bstar,
                MeanMotionDot = satellite.MeanMotionDot,
                MeanMotionDDot = satellite.MeanMotionDDot
            };
        public static Satellite Map(this SatelliteDto satellite) =>
            new Satellite
            {
                Id = satellite.Id,
                Name = satellite.Name,
                InternationalDesignator = satellite.InternationalDesignator,
                Epoch = satellite.Epoch,
                MeanMotion = satellite.MeanMotion,
                Eccentricity = satellite.Eccentricity,
                Inclination = satellite.Inclination,
                RightAscensionOfAscendingNode = satellite.RightAscensionOfAscendingNode,
                ArgOfPericenter = satellite.ArgOfPericenter,
                MeanAnomaly = satellite.MeanAnomaly,
                EphemerisType = satellite.EphemerisType,
                ClassificationType = satellite.ClassificationType,
                ElementNumber = satellite.ElementNumber,
                RevAtEpoch = satellite.RevAtEpoch,
                Bstar = satellite.Bstar,
                MeanMotionDot = satellite.MeanMotionDot,
                MeanMotionDDot = satellite.MeanMotionDDot
            };
    }
}
