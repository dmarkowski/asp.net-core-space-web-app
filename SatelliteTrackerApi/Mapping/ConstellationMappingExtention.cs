﻿using SatelliteTrackerApi.Models;
using SatelliteTrackerShared.Models.Dto;
using System.Linq;

namespace SatelliteTrackerApi.Mapping
{
    public static class ConstellationMappingExtention
    {
        public static ConstellationBasicDto Map(this Constellation constellation) =>
            new ConstellationBasicDto
            {
                Name = constellation.Name,
                Description = constellation.Description,
                Count = constellation.Count
            };

        public static ConstellationDto MapFull(this Constellation constellation) =>
            new ConstellationDto
            {
                Name = constellation.Name,
                Description = constellation.Description,
                Count = constellation.Count,
                Satellites = constellation.Satellites?.Select(_ => _.Map()).ToList()
            };

        public static Constellation Map(this ConstellationDto constellation) =>
            new Constellation
            {
                Name = constellation.Name,
                Description = constellation.Description,
                Count = constellation.Count,
                Satellites = constellation.Satellites?.Select(_ => _.Map()).ToList()
            };
    }
}
