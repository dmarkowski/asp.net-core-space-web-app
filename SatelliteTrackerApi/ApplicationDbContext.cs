﻿using Microsoft.EntityFrameworkCore;
using SatelliteTrackerApi.Models;

namespace SatelliteTrackerApi
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Satellite> Satellites { get; set; }

        public DbSet<Constellation> Constellations { get; set; }
    }
}
