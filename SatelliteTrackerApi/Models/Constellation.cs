﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SatelliteTrackerApi.Models
{
    public class Constellation
    {
        [Key]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        public int Count { get; set; }

        public virtual ICollection<Satellite> Satellites { get; set; }
    }
}
