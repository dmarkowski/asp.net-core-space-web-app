﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SatelliteTrackerApi.Models
{
    public class Satellite
    {
        [Key]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Invalid satellite number")]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string InternationalDesignator { get; set; }

        [Required]
        [StringLength(200)]
        public string Epoch { get; set; }

        [Required]
        public float MeanMotion { get; set; }

        [Required]
        public float Eccentricity { get; set; }

        [Required]
        public float Inclination { get; set; }

        [Required]
        public float RightAscensionOfAscendingNode { get; set; }

        [Required]
        public float ArgOfPericenter { get; set; }

        [Required]
        public float MeanAnomaly { get; set; }

        [Required]
        [Range(0, 9, ErrorMessage = "Invalid ephemeris type")]
        public int EphemerisType { get; set; }

        [Required]
        [StringLength(1)]
        public string ClassificationType { get; set; }

        [Required]
        public int ElementNumber { get; set; }

        [Required]
        public int RevAtEpoch { get; set; }

        [Required]
        public float Bstar { get; set; }

        [Required]
        public float MeanMotionDot { get; set; }

        [Required]
        public float MeanMotionDDot { get; set; }

        public virtual ICollection<Constellation> Constellations { get; set; }
    }
}
