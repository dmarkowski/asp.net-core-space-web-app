﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SatelliteTrackerApi.Mapping;
using SatelliteTrackerApi.Repository;
using SatelliteTrackerShared.Models.Dto;

namespace SatelliteTrackerApi.Controllers
{
    [Route("api/constellations")]
    [ApiController]
    public class ConstellationApiController : ControllerBase
    {
        private readonly IConstellationRepository constellationRepository;
        private readonly ISatelliteRepository satelliteRepository;

        public ConstellationApiController(IConstellationRepository constellationRepository, ISatelliteRepository satelliteRepository)
        {
            this.constellationRepository = constellationRepository ?? throw new ArgumentNullException(nameof(constellationRepository));
            this.satelliteRepository = satelliteRepository ?? throw new ArgumentNullException(nameof(satelliteRepository));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ConstellationBasicDto>>> Get()
        {
            var constellations = await this.constellationRepository.Get();
            if (constellations == null)
                return this.NotFound();
            return this.Ok(constellations.Select(_ => _.Map()).ToList());
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<ConstellationDto>> Get(string name)
        {
            var constellation = await this.constellationRepository.Get(name);
            if (constellation == null)
                return this.NotFound();
            return this.Ok(constellation.MapFull());
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult> Update(ConstellationDto constellation)
        {
            foreach (var sat in constellation.Satellites)
                await this.satelliteRepository.Update(sat.Map());

            await this.constellationRepository.Update(constellation.Name, constellation.Description, constellation.Count, constellation.Satellites.Select(_ => _.Id));
            return this.Ok();
        }
    }
}
