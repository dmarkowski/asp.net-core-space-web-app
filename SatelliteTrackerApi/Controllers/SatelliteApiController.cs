﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SatelliteTrackerApi.Mapping;
using SatelliteTrackerApi.Repository;
using SatelliteTrackerShared.Models.Dto;

namespace SatelliteTrackerApi.Controllers
{
    [Route("api/satellites")]
    [ApiController]
    public class SatelliteApiController : ControllerBase
    {
        private readonly ISatelliteRepository repository;

        public SatelliteApiController(ISatelliteRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<SatelliteDto>>> Get()
        {
            var satellites = await this.repository.Get();
            if (satellites == null || !satellites.Any())
                return this.NotFound();
            return this.Ok(satellites.Select(_ => _.Map()).ToList());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SatelliteDto>> Get(int id)
        {
            var satellite = await this.repository.Get(id);
            if (satellite == null)
                return this.NotFound();
            return this.Ok(satellite.Map());
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult> Update(SatelliteDto satellite)
        {
            await this.repository.Update(satellite.Map());
            return this.Ok();
        }
    }
}
