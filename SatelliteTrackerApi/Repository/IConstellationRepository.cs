﻿using SatelliteTrackerApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SatelliteTrackerApi.Repository
{
    public interface IConstellationRepository
    {
        public Task<Constellation> Get(string name);
        public Task<List<Constellation>> Get();
        public Task<bool> IsExist(string name);
        Task Update(string name, string description, int count, IEnumerable<int> satellitesIds);
    }
}
