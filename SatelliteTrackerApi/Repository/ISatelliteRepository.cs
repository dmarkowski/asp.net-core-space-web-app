﻿using SatelliteTrackerApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SatelliteTrackerApi.Repository
{
    public interface ISatelliteRepository
    {
        public Task<List<Satellite>> Get();
        public Task Update(Satellite satellite);
        public Task<Satellite> Get(int id);
    }
}
