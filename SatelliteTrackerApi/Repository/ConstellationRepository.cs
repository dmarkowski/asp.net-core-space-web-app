﻿using Microsoft.EntityFrameworkCore;
using SatelliteTrackerApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SatelliteTrackerApi.Repository
{
    public class ConstellationRepository : IConstellationRepository
    {
        private readonly ApplicationDbContext context;

        public ConstellationRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public Task<Constellation> Get(string name)
        {
            return this.context.Constellations.Include(_ => _.Satellites).FirstOrDefaultAsync(_ => _.Name == name);
        }

        public Task<bool> IsExist(string name)
        {
            return this.context.Constellations.AnyAsync(_ => _.Name == name);
        }

        public Task<List<Constellation>> Get()
        {
            return this.context.Constellations.ToListAsync();
        }

        public Task Update(string name, string description, int count, IEnumerable<int> satellitesIds)
        {
            var constellation = this.context.Constellations.FirstOrDefault(_ => _.Name == name);
            var satellites = satellitesIds.Select(id => this.context.Satellites.First(_ => _.Id == id)).ToList();
            if (constellation == null)
            {
                this.context.Constellations.Add(
                    new Constellation
                    {
                        Name = name,
                        Satellites = satellites,
                        Description = description,
                        Count = count
                    });
            }
            else
            {
                var entry = this.context.Entry(constellation);
                entry.Collection(_ => _.Satellites).Load();
                constellation.Satellites = satellites;
                constellation.Description = description;
                constellation.Count = count;
            }
            return this.context.SaveChangesAsync();
        }
    }
}
