﻿using Microsoft.EntityFrameworkCore;
using SatelliteTrackerApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SatelliteTrackerApi.Repository
{
    public class SatelliteRepository : ISatelliteRepository
    {
        private readonly ApplicationDbContext context;

        public SatelliteRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public Task<List<Satellite>> Get()
        {
            return this.context.Satellites.ToListAsync();
        }

        public Task Update(Satellite entity)
        {
            var isExist = this.context.Satellites.Any(_ => _.Id == entity.Id);
            if (isExist)
                this.context.Entry(entity).State = EntityState.Modified;
            else
                this.context.Satellites.Add(entity);
            return this.context.SaveChangesAsync();
        }

        public Task<Satellite> Get(int id)
        {
            return this.context.Satellites.FirstOrDefaultAsync(_ =>_.Id == id);
        }
    }
}
