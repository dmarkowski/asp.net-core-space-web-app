﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SatelliteTrackerApi.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Constellations",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Constellations", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Satellites",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false),
                    InternationalDesignator = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false),
                    Epoch = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false),
                    MeanMotion = table.Column<float>(type: "REAL", nullable: false),
                    Eccentricity = table.Column<float>(type: "REAL", nullable: false),
                    Inclination = table.Column<float>(type: "REAL", nullable: false),
                    RightAscensionOfAscendingNode = table.Column<float>(type: "REAL", nullable: false),
                    ArgOfPericenter = table.Column<float>(type: "REAL", nullable: false),
                    MeanAnomaly = table.Column<float>(type: "REAL", nullable: false),
                    EphemerisType = table.Column<int>(type: "INTEGER", nullable: false),
                    ClassificationType = table.Column<string>(type: "TEXT", maxLength: 1, nullable: false),
                    ElementNumber = table.Column<int>(type: "INTEGER", nullable: false),
                    RevAtEpoch = table.Column<int>(type: "INTEGER", nullable: false),
                    Bstar = table.Column<float>(type: "REAL", nullable: false),
                    MeanMotionDot = table.Column<float>(type: "REAL", nullable: false),
                    MeanMotionDDot = table.Column<float>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Satellites", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConstellationSatellite",
                columns: table => new
                {
                    ConstellationsName = table.Column<string>(type: "TEXT", nullable: false),
                    SatellitesId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConstellationSatellite", x => new { x.ConstellationsName, x.SatellitesId });
                    table.ForeignKey(
                        name: "FK_ConstellationSatellite_Constellations_ConstellationsName",
                        column: x => x.ConstellationsName,
                        principalTable: "Constellations",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ConstellationSatellite_Satellites_SatellitesId",
                        column: x => x.SatellitesId,
                        principalTable: "Satellites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConstellationSatellite_SatellitesId",
                table: "ConstellationSatellite",
                column: "SatellitesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConstellationSatellite");

            migrationBuilder.DropTable(
                name: "Constellations");

            migrationBuilder.DropTable(
                name: "Satellites");
        }
    }
}
