﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SatelliteTrackerApi.Migrations
{
    public partial class ConstellationUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "Constellations",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Constellations",
                type: "TEXT",
                maxLength: 1000,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "Constellations");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Constellations");
        }
    }
}
