﻿using Newtonsoft.Json;
using SatelliteTrackerShared.Models.Dto;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInitConsoleApp
{
    class Program
    {
        private static readonly ConstellationSetting[] constellations = new[] {
            new ConstellationSetting { Name = "active", Description = "Active satellites", Url = "https://celestrak.com/NORAD/elements/gp.php?GROUP=active&FORMAT=json" },
            new ConstellationSetting { Name = "stations", Description = "Space stations", Url = "https://celestrak.com/NORAD/elements/gp.php?GROUP=stations&FORMAT=json" },
            new ConstellationSetting { Name = "rocket-bodies", Description = "Rocket bodies", Url = "https://celestrak.com/NORAD/elements/gp.php?NAME=r/b&FORMAT=json" },
            new ConstellationSetting { Name = "last-30-days", Description = "Last 30 Days' Launches", Url = "https://celestrak.com/NORAD/elements/gp.php?GROUP=last-30-days&FORMAT=json" },
            new ConstellationSetting { Name = "debris", Description = "Debris on orbit", Url = "https://celestrak.com/NORAD/elements/gp.php?NAME=deb&FORMAT=json" },
        };

        private static readonly string ApiPostEndpointUrl = "https://localhost:44331/api/constellations";

        private static async Task<SatelliteDto[]> GetSatellitesFromExternalApiAsync(string url)
        {
            var client = new HttpClient();
            var response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var satellites = JsonConvert.DeserializeObject<SatelliteDto[]>(content);
            if (satellites == null || satellites.Length == 0)
                throw new Exception($"Get request {url} returned empty satellites collection");
            return satellites;
        }

        private static async Task PostAsyncConstellationToApi(ConstellationSetting constellationSetting, SatelliteDto[] satellites)
        {
            var stringPayload = JsonConvert.SerializeObject(new ConstellationDto
            {
                Name = constellationSetting.Name,
                Description = constellationSetting.Description,
                Count = satellites.Length,
                Satellites = satellites
            });
            var content = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(180);
            var response = await client.PostAsync(ApiPostEndpointUrl, content);
            response.EnsureSuccessStatusCode();
        }

        static async Task Main(string[] args)
        {
            foreach (var constellation in constellations)
            {
                try
                {
                    Console.WriteLine($"START - Constellation {constellation.Name}");
                    var satellites = await GetSatellitesFromExternalApiAsync(constellation.Url);
                    Console.WriteLine($"IN PROGRESS - Constellation {constellation.Name} - Response from external API with {satellites.Length} satellites");
                    await PostAsyncConstellationToApi(constellation, satellites);
                    Console.WriteLine($"END - Constellation {constellation.Name} - Posted constellation {constellation.Name} with {satellites.Length} satellites");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"FAILED - Constellation {constellation.Name}");
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
