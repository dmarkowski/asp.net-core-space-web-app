﻿using Newtonsoft.Json;

namespace SatelliteTrackerShared.Models.Dto
{
    public class ConstellationBasicDto
    {
        [JsonProperty("name")]
        [JsonRequired]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
        
        [JsonProperty("count")]
        [JsonRequired]
        public int Count { get; set; }
    }
}
