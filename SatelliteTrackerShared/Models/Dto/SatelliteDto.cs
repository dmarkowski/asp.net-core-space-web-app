﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace SatelliteTrackerShared.Models.Dto
{
    public class SatelliteDto
    {
        [JsonProperty("NORAD_CAT_ID")]
        [JsonRequired]
        [Range(1, int.MaxValue, ErrorMessage = "Invalid satellite number")]
        public int Id { get; set; }

        [JsonProperty("OBJECT_NAME")]
        [JsonRequired]
        [StringLength(200, MinimumLength = 1)]
        public string Name { get; set; }

        [JsonProperty("OBJECT_ID")]
        [JsonRequired]
        [StringLength(200, MinimumLength = 1)]
        public string InternationalDesignator { get; set; }

        [JsonProperty("EPOCH")]
        [JsonRequired]
        [StringLength(200, MinimumLength = 1)]
        public string Epoch { get; set; }

        [JsonProperty("MEAN_MOTION")]
        [JsonRequired]
        public float MeanMotion { get; set; }

        [JsonProperty("ECCENTRICITY")]
        [JsonRequired]
        public float Eccentricity { get; set; }

        [JsonProperty("INCLINATION")]
        [JsonRequired]
        public float Inclination { get; set; }

        [JsonProperty("RA_OF_ASC_NODE")]
        [JsonRequired]
        public float RightAscensionOfAscendingNode { get; set; }

        [JsonProperty("ARG_OF_PERICENTER")]
        [JsonRequired]
        public float ArgOfPericenter { get; set; }

        [JsonProperty("MEAN_ANOMALY")]
        [JsonRequired]
        public float MeanAnomaly { get; set; }

        [JsonProperty("EPHEMERIS_TYPE")]
        [JsonRequired]
        [Range(0, 9, ErrorMessage = "Invalid ephemeris type")]
        public int EphemerisType { get; set; }

        [JsonProperty("CLASSIFICATION_TYPE")]
        [JsonRequired]
        [StringLength(1, MinimumLength = 1)]
        public string ClassificationType { get; set; }

        [JsonProperty("ELEMENT_SET_NO")]
        [JsonRequired]
        public int ElementNumber { get; set; }

        [JsonProperty("REV_AT_EPOCH")]
        [JsonRequired]
        public int RevAtEpoch { get; set; }

        [JsonProperty("BSTAR")]
        [JsonRequired]
        public float Bstar { get; set; }

        [JsonProperty("MEAN_MOTION_DOT")]
        [JsonRequired]
        public float MeanMotionDot { get; set; }

        [JsonProperty("MEAN_MOTION_DDOT")]
        [JsonRequired]
        public float MeanMotionDDot { get; set; }
    }
}
