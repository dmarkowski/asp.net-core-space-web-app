﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SatelliteTrackerShared.Models.Dto
{
    public class ConstellationDto : ConstellationBasicDto
    {
        [JsonProperty("satellites")]
        [JsonRequired]
        public ICollection<SatelliteDto> Satellites { get; set; }
    }
}
