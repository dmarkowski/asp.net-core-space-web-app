﻿using SatelliteTrackerApi.Models;
using SatelliteTrackerShared.Models.Dto;

namespace SpaceWebApp.Mapping
{
    public static class SatelliteMappingExtention
    {
        public static SatelliteApiModel Map(this SatelliteDto satellite) =>
            new SatelliteApiModel
            {
                Id = satellite.Id,
                Name = satellite.Name,
                InternationalDesignator = satellite.InternationalDesignator,
                Epoch = satellite.Epoch,
                MeanMotion = satellite.MeanMotion,
                Eccentricity = satellite.Eccentricity,
                Inclination = satellite.Inclination,
                RightAscensionOfAscendingNode = satellite.RightAscensionOfAscendingNode,
                ArgOfPericenter = satellite.ArgOfPericenter,
                MeanAnomaly = satellite.MeanAnomaly,
                EphemerisType = satellite.EphemerisType,
                ClassificationType = satellite.ClassificationType,
                ElementNumber = satellite.ElementNumber,
                RevAtEpoch = satellite.RevAtEpoch,
                Bstar = satellite.Bstar,
                MeanMotionDot = satellite.MeanMotionDot,
                MeanMotionDDot = satellite.MeanMotionDDot
            };
    }
}
