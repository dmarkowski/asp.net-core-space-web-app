﻿using SatelliteTrackerShared.Models.Dto;
using SpaceWebApp.Models;
using System.Linq;

namespace SpaceWebApp.Mapping
{
    public static class ConstellationMappingExtention
    {
        public static ConstellationApiModel Map(this ConstellationDto constellation) =>
            new ConstellationApiModel
            {
                Name = constellation.Name,
                Description = constellation.Description,
                Count = constellation.Count,
                Satellites = constellation.Satellites?.Select(_ => _.Map()).ToList()
            };
    }
}
