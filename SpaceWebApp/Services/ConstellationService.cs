﻿using SatelliteTrackerShared.Models.Dto;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace SpaceWebApp.Services
{
    public class ConstellationService : BaseHttpClientService, IConstellationService
    {

        private readonly string BaseUrl = "/api/constellations";

        public ConstellationService(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<IEnumerable<ConstellationBasicDto>> Get()
        {
            return await this.GetAsync<IEnumerable<ConstellationBasicDto>>(this.BaseUrl);
        }

        public async Task<ConstellationDto> Get(string name)
        {
            return await this.GetAsync<ConstellationDto>($"{this.BaseUrl}/{name}");
        }
    }
}
