﻿using SpaceWebApp.Models;
using System.Collections.Generic;

namespace SpaceWebApp.Services
{
    public interface IMarsRoverService
    {
        public IAsyncEnumerable<RoverApiModel> Get();
    }
}