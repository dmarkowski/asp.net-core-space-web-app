﻿using Microsoft.Extensions.Configuration;
using SpaceWebApp.Models;
using SpaceWebApp.Models.NasaApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SpaceWebApp.Services
{
    public class MarsRoverService : BaseHttpClientService, IMarsRoverService
    {
        private readonly string BaseUrl = "mars-photos/api/v1";

        private readonly IConfiguration configuration;

        public MarsRoverService(HttpClient httpClient, IConfiguration configuration) : base(httpClient)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public async IAsyncEnumerable<RoverApiModel> Get()
        {
            var roversNames = this.GetRoversNames();

            foreach (var rover in roversNames)
            {
                var manifest = await this.GetManifest(rover);
                if (manifest != null)
                {
                    var lastPhotoSol = manifest.Photos?.Max(_ => _.Sol) ?? 0;
                    var photos = await this.GetPhotos(rover, lastPhotoSol);
                    yield return new RoverApiModel
                    {
                        LandingDate = manifest.LandingDate,
                        LaunchDate = manifest.LaunchDate,
                        MaxDate = manifest.MaxDate,
                        MaxSol = manifest.MaxSol,
                        Name = manifest.Name,
                        Status = manifest.Status,
                        TotalPhotos = manifest.TotalPhotos,
                        LastPhotos = photos?.ToList()
                    };
                }

            }
        }

        private IEnumerable<string> GetRoversNames()
        {
            return this.configuration.GetSection("Rovers").Get<IEnumerable<string>>();
        }

        private string GetApiKey()
        {
            return this.configuration["NasaApiKey"];
        }

        private async Task<RoverManifest> GetManifest(string name)
        {
            var url = $"{this.BaseUrl}/manifests/{name}?api_key={this.GetApiKey()}";
            var response = await this.GetAsync<RoverManifestResponse>(url);
            return response?.RoverManifest;
        }

        private async Task<IEnumerable<PhotoApiModel>> GetPhotos(string roverName, int sol)
        {
            var url = $"{this.BaseUrl}/rovers/{roverName}/photos?sol={sol}&api_key={this.GetApiKey()}";
            var response = await this.GetAsync<PhotosResponse>(url);
            return response?.Photos?.Select(p => new PhotoApiModel
            {
                Sol = p.Sol,
                Camera = new CameraApiModel
                {
                    ShortName = p.Camera?.Name,
                    FullName = p.Camera?.FullName
                },
                EarthDay = p.EarthDate,
                ImageUrl = p.ImageUrl
            });
        }
    }
}