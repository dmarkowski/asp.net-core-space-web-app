﻿using SatelliteTrackerShared.Models.Dto;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace SpaceWebApp.Services
{
    public class SatelliteService : BaseHttpClientService, ISatelliteService
    {

        private readonly string BaseUrl = "/api/satellites";

        public SatelliteService(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<IEnumerable<SatelliteDto>> Get()
        {
            return await this.GetAsync<IEnumerable<SatelliteDto>>(this.BaseUrl);
        }

        public async Task<SatelliteDto> Get(int id)
        {
            return await this.GetAsync<SatelliteDto>($"{this.BaseUrl}/{id}");
        }
    }
}
