﻿using SatelliteTrackerShared.Models.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpaceWebApp.Services
{
    public interface ISatelliteService
    {
        public Task<IEnumerable<SatelliteDto>> Get();

        public Task<SatelliteDto> Get(int id);
    }
}