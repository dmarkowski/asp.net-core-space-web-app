﻿using SatelliteTrackerShared.Models.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpaceWebApp.Services
{
    public interface IConstellationService
    {
        public Task<IEnumerable<ConstellationBasicDto>> Get();

        public Task<ConstellationDto> Get(string name);
    }
}