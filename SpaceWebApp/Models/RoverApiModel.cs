﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SpaceWebApp.Models
{
    public class RoverApiModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("landingDate")]
        public string LandingDate { get; set; }

        [JsonProperty("launchDate")]
        public string LaunchDate { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("maxSol")]
        public int MaxSol { get; set; }

        [JsonProperty("maxDate")]
        public string MaxDate { get; set; }

        [JsonProperty("totalPhotos")]
        public int TotalPhotos { get; set; }

        [JsonProperty("lastPhotos")]
        public ICollection<PhotoApiModel> LastPhotos { get; set; }
    }
}
