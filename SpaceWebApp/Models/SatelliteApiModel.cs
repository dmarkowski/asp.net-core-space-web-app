﻿using Newtonsoft.Json;

namespace SatelliteTrackerApi.Models
{
    public class SatelliteApiModel
    {
        [JsonProperty("noradCatalogId")]
        [JsonRequired]
        public int Id { get; set; }

        [JsonProperty("objectName")]
        [JsonRequired]
        public string Name { get; set; }

        [JsonProperty("internationalDesignator")]
        [JsonRequired]
        public string InternationalDesignator { get; set; }

        [JsonProperty("epoch")]
        [JsonRequired]
        public string Epoch { get; set; }

        [JsonProperty("meanMotion")]
        [JsonRequired]
        public float MeanMotion { get; set; }

        [JsonProperty("eccentricity")]
        [JsonRequired]
        public float Eccentricity { get; set; }

        [JsonProperty("inclination")]
        [JsonRequired]
        public float Inclination { get; set; }

        [JsonProperty("rightAscensionOfAscendingNode")]
        [JsonRequired]
        public float RightAscensionOfAscendingNode { get; set; }

        [JsonProperty("argOfPericenter")]
        [JsonRequired]
        public float ArgOfPericenter { get; set; }

        [JsonProperty("meanAnomaly")]
        [JsonRequired]
        public float MeanAnomaly { get; set; }

        [JsonProperty("ephemerisType")]
        [JsonRequired]
        public int EphemerisType { get; set; }

        [JsonProperty("classificationType")]
        [JsonRequired]
        public string ClassificationType { get; set; }

        [JsonProperty("elementNumber")]
        [JsonRequired]
        public int ElementNumber { get; set; }

        [JsonProperty("revAtEpoch")]
        [JsonRequired]
        public int RevAtEpoch { get; set; }

        [JsonProperty("bstar")]
        [JsonRequired]
        public float Bstar { get; set; }

        [JsonProperty("meanMotionDot")]
        [JsonRequired]
        public float MeanMotionDot { get; set; }

        [JsonProperty("MEAN_MOTION_DDOT")]
        [JsonRequired]
        public float MeanMotionDDot { get; set; }
    }
}
