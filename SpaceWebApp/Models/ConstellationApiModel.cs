﻿using Newtonsoft.Json;
using SatelliteTrackerApi.Models;
using SatelliteTrackerShared.Models.Dto;
using System.Collections.Generic;


namespace SpaceWebApp.Models
{
    public class ConstellationApiModel : ConstellationBasicDto
    {
        [JsonProperty("satellites")]
        [JsonRequired]
        public ICollection<SatelliteApiModel> Satellites { get; set; }
    }
}
