﻿using Newtonsoft.Json;

namespace SpaceWebApp.Models
{
    public class CameraApiModel
    {
        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }
    }
}