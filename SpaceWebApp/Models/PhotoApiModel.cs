﻿using Newtonsoft.Json;

namespace SpaceWebApp.Models
{
    public class PhotoApiModel
    {
        [JsonProperty("camera")]
        public CameraApiModel Camera { get; set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("sol")]
        public int Sol { get; set; }

        [JsonProperty("earthDay")]
        public string EarthDay { get; set; }
    }
}
