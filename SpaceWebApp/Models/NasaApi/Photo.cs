﻿using Newtonsoft.Json;

namespace SpaceWebApp.Models.NasaApi
{
    public class Photo
    {
        [JsonProperty("img_src")]
        public string ImageUrl { get; set; }

        [JsonProperty("sol")]
        public int Sol { get; set; }

        [JsonProperty("earth_date")]
        public string EarthDate { get; set; }

        [JsonProperty("camera")]
        public Camera Camera { get; set; }
    }
}
