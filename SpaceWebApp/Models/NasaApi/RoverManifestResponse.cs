﻿using Newtonsoft.Json;

namespace SpaceWebApp.Models.NasaApi
{
    public class RoverManifestResponse
    {
        [JsonProperty("photo_manifest")]
        public RoverManifest RoverManifest { get; set; }
    }
}
