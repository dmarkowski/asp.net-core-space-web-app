﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SpaceWebApp.Models.NasaApi
{
    public class RoverManifest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("landing_date")]
        public string LandingDate { get; set; }

        [JsonProperty("launch_date")]
        public string LaunchDate { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("max_sol")]
        public int MaxSol { get; set; }

        [JsonProperty("max_date")]
        public string MaxDate { get; set; }

        [JsonProperty("total_photos")]
        public int TotalPhotos { get; set; }

        [JsonProperty("photos")]
        public ICollection<RoverManifestPhoto> Photos { get; set; }
    }
}
