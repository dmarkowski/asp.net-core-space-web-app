﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SpaceWebApp.Models.NasaApi
{
    public class RoverManifestPhoto
    {
        [JsonProperty("sol")]
        public int Sol { get; set; }

        [JsonProperty("earth_date")]
        public string EarthDate { get; set; }

        [JsonProperty("total_photos")]
        public int TotalPhotos { get; set; }

        [JsonProperty("cameras")]
        public ICollection<string> cameras { get; set; }
    }
}