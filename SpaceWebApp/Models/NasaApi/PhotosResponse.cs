﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SpaceWebApp.Models.NasaApi
{
    public class PhotosResponse
    {
        [JsonProperty("photos")]
        public IEnumerable<Photo> Photos { get; set; }
    }
}
