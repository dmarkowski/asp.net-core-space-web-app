﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpaceWebApp.Models;
using SpaceWebApp.Services;

namespace SatelliteTrackerApi.Controllers
{
    [Route("api/marsrovers")]
    [ApiController]
    public class MarsRoversApiController : ControllerBase
    {
        private readonly IMarsRoverService service;

        public MarsRoversApiController(IMarsRoverService service)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
        }

        [HttpGet]
        public ActionResult<IAsyncEnumerable<RoverApiModel>> Get()
        {
            var rovers = this.service.Get();
            return this.Ok(rovers);
        }
    }
}
