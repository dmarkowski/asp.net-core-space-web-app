﻿using Microsoft.AspNetCore.Mvc;
using SatelliteTrackerShared.Models.Dto;
using SpaceWebApp.Mapping;
using SpaceWebApp.Models;
using SpaceWebApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpaceWebApp.Controllers
{
    [Route("api/constellations")]
    [ApiController]
    public class ConstellationApiController : ControllerBase
    {
        private readonly IConstellationService constellationService;

        public ConstellationApiController(IConstellationService constellationService)
        {
            this.constellationService = constellationService ?? throw new ArgumentNullException(nameof(constellationService));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ConstellationBasicDto>>> Get()
        {
            var constellations = await this.constellationService.Get();
            if (constellations == null || !constellations.Any())
                return this.NotFound();
            return this.Ok(constellations.ToList());
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<ConstellationApiModel>> Get(string name)
        {
            var constellation = await this.constellationService.Get(name);
            if (constellation == null)
                return this.NotFound();
            return this.Ok(constellation.Map());
        }
    }
}
