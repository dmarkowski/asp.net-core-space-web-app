﻿using Microsoft.AspNetCore.Mvc;
using SatelliteTrackerApi.Models;
using SpaceWebApp.Mapping;
using SpaceWebApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpaceWebApp.Controllers
{
    [Route("api/satellites")]
    [ApiController]
    public class SatelliteApiController : ControllerBase
    {
        private readonly ISatelliteService satelliteService;

        public SatelliteApiController(ISatelliteService satelliteService)
        {
            this.satelliteService = satelliteService ?? throw new ArgumentNullException(nameof(satelliteService));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<SatelliteApiModel>>> Get()
        {
            var satellites = await this.satelliteService.Get();
            if (satellites == null || !satellites.Any())
                return this.NotFound();
            return this.Ok(satellites.Select(_=>_.Map()).ToList());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SatelliteApiModel>> Get(int id)
        {
            var satellite = await this.satelliteService.Get(id);
            if (satellite == null)
                return this.NotFound();
            return this.Ok(satellite.Map());
        }
    }
}
