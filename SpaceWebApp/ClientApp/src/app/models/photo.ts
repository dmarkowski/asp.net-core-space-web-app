import { Camera } from "./camera";

export interface Photo {
    camera: Camera;
    imageUrl: string;
    sol: number;
    earthDay: string
}
