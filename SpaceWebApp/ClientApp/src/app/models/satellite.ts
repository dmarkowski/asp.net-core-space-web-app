export interface Satellite {
    noradCatalogId: number;
    objectName: string;
    internationalDesignator: string;
    epoch: string;
    meanMotion: number;
    eccentricity: number;
    inclination: number;
    rightAscensionOfAscendingNode: number;
    argOfPericenter: number;
    meanAnomaly: number;
    ephemerisType: number;
    classificationType: string;
    elementNumber: number;
    revAtEpoch: number;
    bstar: number;
    meanMotionDot: number;
    meanMotionDDot: number;
}
