export interface SatelliteConstellationBasic {
    name: string;
    count: number;
    description: string;
}
