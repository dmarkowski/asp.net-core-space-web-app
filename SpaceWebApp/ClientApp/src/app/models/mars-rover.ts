import { Photo } from "./photo";

export interface MarsRover {
    name: string;
    landingDate: string;
    launchDate: string;
    status: string;
    maxSol: number;
    maxDate: string;
    totalPhotos: number;
    lastPhotos: Photo[];
}
