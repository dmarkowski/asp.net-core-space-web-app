import { Satellite } from "./satellite";
import { SatelliteConstellationBasic } from "./satellite-constellation-basic";

export interface SatelliteConstellation extends SatelliteConstellationBasic {
    satellites: Satellite[];
}
