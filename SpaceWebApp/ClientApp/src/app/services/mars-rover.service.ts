import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MarsRover } from '../models/mars-rover';

@Injectable({
  providedIn: 'root'
})
export class MarsRoverService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  get(): Observable<MarsRover[]> {
    return this.http.get<MarsRover[]>(`${this.baseUrl}api/marsrovers`);
  }
}
