import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SatelliteConstellation } from '../models/satellite-constellation';
import { SatelliteConstellationBasic } from '../models/satellite-constellation-basic';

@Injectable()
export class SatelliteConstellationService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  getConstellations(): Observable<SatelliteConstellationBasic[]> {
    return this.http.get<SatelliteConstellationBasic[]>(`${this.baseUrl}api/constellations`);
  }

  getConstellation(name: string): Observable<SatelliteConstellation> {
    return this.http.get<SatelliteConstellation>(`${this.baseUrl}api/constellations/${name}`);
  }
}
