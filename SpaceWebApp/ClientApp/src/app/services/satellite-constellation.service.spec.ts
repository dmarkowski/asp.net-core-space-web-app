import { TestBed } from '@angular/core/testing';

import { SatelliteConstellationService } from './satellite-constellation.service';

describe('SatelliteConstellationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SatelliteConstellationService = TestBed.get(SatelliteConstellationService);
    expect(service).toBeTruthy();
  });
});
