import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { HomeComponent } from './components/home/home.component';
import { SatellitesComponent } from './components/satellites/satellites.component';
import { ConstellationsComponent } from './components/constellations/constellations.component';
import { SatelliteConstellationService } from './services/satellite-constellation.service';
import { ConstellationDetailsComponent } from './components/constellation-details/constellation-details.component';
import { MarsRoverComponent } from './components/mars-rover/mars-rover.component';
import { MarsRoversComponent } from './components/mars-rovers/mars-rovers.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    SatellitesComponent,
    ConstellationsComponent,
    ConstellationDetailsComponent,
    MarsRoverComponent,
    MarsRoversComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'constellations', component: ConstellationsComponent },
      { path: 'marsrovers', component: MarsRoversComponent },
      { path: 'constellations/:name', component: ConstellationDetailsComponent },
    ])
  ],
  providers: [
    SatelliteConstellationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
