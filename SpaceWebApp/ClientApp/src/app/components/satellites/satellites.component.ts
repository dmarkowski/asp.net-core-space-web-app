import { Component, Input, OnInit } from '@angular/core';
import { Satellite } from 'src/app/models/satellite';
import { SatelliteConstellationService } from 'src/app/services/satellite-constellation.service';

@Component({
  selector: 'app-satellites',
  templateUrl: './satellites.component.html'
})
export class SatellitesComponent {

  @Input() set satellites(value: Satellite[]) {
    if (value) {
      this.allSatellites = value;
      this.filterResult = value;
    }
  }

  filterResult: Satellite[] = [];
  allSatellites: Satellite[] = [];

  constructor(private service: SatelliteConstellationService) { }

  filter(value: string): void {
    console.log(value);
    this.filterResult = this.allSatellites.filter(sat => {
      return (sat.noradCatalogId && sat.noradCatalogId.toString().includes(value))
        || (sat.objectName && sat.objectName.includes(value))
        || (sat.elementNumber && sat.elementNumber.toString().includes(value))
        || (sat.internationalDesignator && sat.internationalDesignator.includes(value))
    });
  }
}
