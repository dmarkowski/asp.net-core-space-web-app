import { Component, Input } from '@angular/core';
import { MarsRover } from 'src/app/models/mars-rover';
import { Photo } from 'src/app/models/photo';

@Component({
  selector: 'app-mars-rover',
  templateUrl: './mars-rover.component.html'
})
export class MarsRoverComponent {

  currentPhotoIdx = 0;
  lastPhotoIdx = 0;
  marsRover: MarsRover;

  @Input() set rover(value: MarsRover) {
    if (value) {
      this.marsRover = value;
      if (value.lastPhotos) {
        this.lastPhotoIdx = value.lastPhotos.length - 1;
      }
    }
  }

  get currentPhoto(): Photo {
    if (this.marsRover && this.marsRover.lastPhotos) {
      if (this.currentPhotoIdx > this.marsRover.lastPhotos.length) {
        return this.marsRover.lastPhotos[-1];
      }
      if (this.currentPhotoIdx < 1) {
        return this.marsRover.lastPhotos[0];
      }
      return this.marsRover.lastPhotos[this.currentPhotoIdx];
    }
    return null;
  }

  next(): void {
    this.currentPhotoIdx = this.currentPhotoIdx + 1 > this.lastPhotoIdx ? this.lastPhotoIdx : this.currentPhotoIdx + 1;
  }

  prev(): void {
    this.currentPhotoIdx = this.currentPhotoIdx - 1 < 0 ? 0 : this.currentPhotoIdx - 1;
  }

  constructor() { }

}
