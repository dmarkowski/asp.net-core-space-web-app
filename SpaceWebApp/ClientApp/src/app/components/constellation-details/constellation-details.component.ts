import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SatelliteConstellation } from 'src/app/models/satellite-constellation';
import { SatelliteConstellationService } from 'src/app/services/satellite-constellation.service';

@Component({
  selector: 'app-constellation-details',
  templateUrl: './constellation-details.component.html'
})
export class ConstellationDetailsComponent implements OnInit {

  constellation: SatelliteConstellation;
  isError = false;
  isLoading = true;
  constellationName: string;

  constructor(private route: ActivatedRoute, private service: SatelliteConstellationService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.constellationName = params.get('name')
      this.service.getConstellation(this.constellationName)
        .pipe(catchError(() => {
          this.isError = true;
          return of(null);
        })).subscribe(data => {
          this.constellation = data
          this.isLoading = false;
        });
    })
  }

}
