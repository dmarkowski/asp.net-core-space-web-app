import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MarsRover } from 'src/app/models/mars-rover';
import { MarsRoverService } from 'src/app/services/mars-rover.service';

@Component({
  selector: 'app-mars-rovers',
  templateUrl: './mars-rovers.component.html'
})
export class MarsRoversComponent implements OnInit {

  rovers: MarsRover[] = [];
  isError = false;
  isLoading = true;

  constructor(private service: MarsRoverService) { }

  ngOnInit(): void {
    this.service.get()
      .pipe(
        catchError(() => {
          this.isError = true;
          return of(null);
        }))
      .subscribe(data => {
        this.rovers = data;
        this.isLoading = false;
      });
  }
}
