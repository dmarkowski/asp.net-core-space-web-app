import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SatelliteConstellationBasic } from 'src/app/models/satellite-constellation-basic';
import { SatelliteConstellationService } from 'src/app/services/satellite-constellation.service';

@Component({
  selector: 'app-constellations',
  templateUrl: './constellations.component.html'
})
export class ConstellationsComponent implements OnInit {

  constellations: SatelliteConstellationBasic[] = [];
  isError = false;
  isLoading = true;

  constructor(private service: SatelliteConstellationService) { }

  ngOnInit(): void {
    this.service.getConstellations()
      .pipe(
        catchError(() => {
          this.isError = true;
          return of(null);
        }))
      .subscribe(data => {
        this.constellations = data;
        this.isLoading = false;
      });
  }
}
