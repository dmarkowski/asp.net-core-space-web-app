using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SpaceWebApp.Services;
using System;

namespace SatelliteTrackerWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews().AddNewtonsoftJson();
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
            services.Add(new ServiceDescriptor(typeof(ISatelliteService), typeof(SatelliteService), ServiceLifetime.Transient));
            services.Add(new ServiceDescriptor(typeof(IConstellationService), typeof(ConstellationService), ServiceLifetime.Transient));
            services.Add(new ServiceDescriptor(typeof(IMarsRoverService), typeof(MarsRoverService), ServiceLifetime.Transient));
            services.AddHttpClient<ISatelliteService, SatelliteService>(client =>
            {
                client.BaseAddress = new Uri(Configuration["serviceUrl"]);
            });
            services.AddHttpClient<IConstellationService, ConstellationService>(client =>
            {
                client.BaseAddress = new Uri(Configuration["serviceUrl"]);
            });
            services.AddHttpClient<IMarsRoverService, MarsRoverService>(client =>
            {
                client.BaseAddress = new Uri(Configuration["NasaApiUrl"]);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
