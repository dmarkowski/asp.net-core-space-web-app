# ASP.NET-Core-space-web-app
.NET Core & Angular web system that utilize CelesTrack TLE database and NASA API to collect and display data regarding satellites position and last day images from mars rovers.

## Table of content
* [Technologies](#technologies)
* [Software design](#software-design)
* [Setup](#setup)
* [TODO list](#todo-list)

## Technologies
Following technologies were used in this project:
* ASP.NET Core
* Entity Framework
* Angular 8
* XUnit

## Software design
### Project structure
* [SatelliteTrackerApi](SatelliteTrackerApi/) - ASP.NET Core REST API responsible for updating and fetching data from SQL database.
* [SatelliteTrackerApiTests](SatelliteTrackerApi/) - Project with SatelliteTrackerApi unit tests.
* [SatelliteTrackerShared](SatelliteTrackerShared/) - Shared .NET Standard 2.0 call library with DTO objects.
* [SpaceWebApp](SpaceWebApp/) - ASP.NET Core SPA application with Angular Client-Side application [ClientApp](SpaceWebApp/ClientApp/)
* [SpaceInitConsoleApp](SpaceInitConsoleApp/) - Temporary .NET Core console application used to fetch latest CelesTrack data and populate database.

### Architecture
![architecture](resources/diagram-SpaceWebApp-Solution-Software-Architecture.jpg)

### SQL diagram
![sql diagram](resources/diagram-SQL-Data-Diagram.jpg)

## Setup
1. In Visual Studio select SpaceWebApp and SatelliteTrackerApi as startup projects.
2. Start projects.
3. On first start run SpaceInitConsoleApp to populate data in database.

## TODO list
* Move applications to Docker containers.
* Refactor SpaceInitConsoleApp to Azure function.
* Cake script for application deployment.
* Unit test all .NET projects.
* Unit test Angular application.
