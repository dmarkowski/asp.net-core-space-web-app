using AutoFixture;
using SatelliteTrackerApi.Mapping;
using SatelliteTrackerApi.Models;
using SatelliteTrackerShared.Models.Dto;
using Xunit;

namespace SatelliteTrackerApiTests.Mapping
{
    public class SatelliteMappingExtentionTest
    {
        private readonly IFixture fixture;

        public SatelliteMappingExtentionTest()
        {
            // DB entities have a circular reference due to many-many relationship
            this.fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Fact]
        public void MapSatellite_ShouldReturnMapedSatelliteDto()
        {
            // arrange
            var satellite = this.fixture.Create<Satellite>();

            // act
            var result = satellite.Map();

            // assert
            Assert.IsType<SatelliteDto>(result);
            Assert.Equal(satellite.Name, result.Name);
            Assert.Equal(satellite.Id, result.Id);
            Assert.Equal(satellite.InternationalDesignator, result.InternationalDesignator);
            Assert.Equal(satellite.Epoch, result.Epoch);
            Assert.Equal(satellite.MeanMotion, result.MeanMotion);
            Assert.Equal(satellite.Eccentricity, result.Eccentricity);
            Assert.Equal(satellite.Inclination, result.Inclination);
            Assert.Equal(satellite.RightAscensionOfAscendingNode, result.RightAscensionOfAscendingNode);
            Assert.Equal(satellite.ArgOfPericenter, result.ArgOfPericenter);
            Assert.Equal(satellite.MeanAnomaly, result.MeanAnomaly);
            Assert.Equal(satellite.EphemerisType, result.EphemerisType);
            Assert.Equal(satellite.ClassificationType, result.ClassificationType);
            Assert.Equal(satellite.ElementNumber, result.ElementNumber);
            Assert.Equal(satellite.RevAtEpoch, result.RevAtEpoch);
            Assert.Equal(satellite.Bstar, result.Bstar);
            Assert.Equal(satellite.MeanMotionDot, result.MeanMotionDot);
            Assert.Equal(satellite.MeanMotionDDot, result.MeanMotionDDot);
        }

        [Fact]
        public void MapDtoSatellite_ShouldReturnMapedSatellite()
        {
            // arrange
            var satellite = this.fixture.Create<SatelliteDto>();

            // act
            var result = satellite.Map();

            // assert
            Assert.IsType<Satellite>(result);
            Assert.Equal(satellite.Name, result.Name);
            Assert.Equal(satellite.Id, result.Id);
            Assert.Equal(satellite.InternationalDesignator, result.InternationalDesignator);
            Assert.Equal(satellite.Epoch, result.Epoch);
            Assert.Equal(satellite.MeanMotion, result.MeanMotion);
            Assert.Equal(satellite.Eccentricity, result.Eccentricity);
            Assert.Equal(satellite.Inclination, result.Inclination);
            Assert.Equal(satellite.RightAscensionOfAscendingNode, result.RightAscensionOfAscendingNode);
            Assert.Equal(satellite.ArgOfPericenter, result.ArgOfPericenter);
            Assert.Equal(satellite.MeanAnomaly, result.MeanAnomaly);
            Assert.Equal(satellite.EphemerisType, result.EphemerisType);
            Assert.Equal(satellite.ClassificationType, result.ClassificationType);
            Assert.Equal(satellite.ElementNumber, result.ElementNumber);
            Assert.Equal(satellite.RevAtEpoch, result.RevAtEpoch);
            Assert.Equal(satellite.Bstar, result.Bstar);
            Assert.Equal(satellite.MeanMotionDot, result.MeanMotionDot);
            Assert.Equal(satellite.MeanMotionDDot, result.MeanMotionDDot);
        }
    }
}
