using AutoFixture;
using SatelliteTrackerApi.Mapping;
using SatelliteTrackerApi.Models;
using SatelliteTrackerShared.Models.Dto;
using Xunit;

namespace SatelliteTrackerApiTests.Mapping
{
    public class ConstellationMappingExtentionTest
    {
        private readonly IFixture fixture;

        public ConstellationMappingExtentionTest()
        {
            // DB entities have a circular reference due to many-many relationship
            this.fixture = new Fixture();
            fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Fact]
        public void MapFullConstellation_IfSatellitesNull_ShouldReturnConstellationDtoWithNullSatellites()
        {
            // arrange
            var constellation = new Constellation { Name = "Name" };

            // act
            var result = constellation.MapFull();

            // assert
            Assert.IsType<ConstellationDto>(result);
            Assert.Equal(constellation.Name, result.Name);
            Assert.Equal(constellation.Description, result.Description);
            Assert.Equal(constellation.Count, result.Count);
            Assert.Null(result.Satellites);
        }

        [Fact]
        public void MapConstellation_ShouldReturnMapedConstellationDto()
        {
            // arrange
            var constellation = this.fixture.Create<Constellation>();

            // act
            var result = constellation.Map();

            // assert
            Assert.IsType<ConstellationBasicDto>(result);
            Assert.Equal(constellation.Name, result.Name);
            Assert.Equal(constellation.Description, result.Description);
            Assert.Equal(constellation.Count, result.Count);
        }

        [Fact]
        public void MapFullConstellation_ShouldReturnMapedConstellationDto()
        {
            // arrange
            var constellation = this.fixture.Create<Constellation>();

            // act
            var result = constellation.MapFull();

            // assert
            Assert.IsType<ConstellationDto>(result);
            Assert.Equal(constellation.Name, result.Name);
            Assert.Equal(constellation.Description, result.Description);
            Assert.Equal(constellation.Count, result.Count);
            Assert.NotNull(result.Satellites);
            Assert.Equal(constellation.Satellites.Count, result.Satellites.Count);
        }

        [Fact]
        public void MapConstellationDto_IfSatellitesNull_ShouldReturnConstellationWithNullSatellites()
        {
            // arrange
            var constellation = new ConstellationDto { Name = "Name" };

            // act
            var result = constellation.Map();

            // assert
            Assert.IsType<Constellation>(result);
            Assert.Equal(constellation.Name, result.Name);
            Assert.Equal(constellation.Description, result.Description);
            Assert.Equal(constellation.Count, result.Count);
            Assert.Null(result.Satellites);
        }

        [Fact]
        public void MapConstellationDto_ShouldReturnMapedConstellationDto()
        {
            // arrange
            var constellation = this.fixture.Create<ConstellationDto>();

            // act
            var result = constellation.Map();

            // assert
            Assert.IsType<Constellation>(result);
            Assert.Equal(constellation.Name, result.Name);
            Assert.Equal(constellation.Description, result.Description);
            Assert.Equal(constellation.Count, result.Count);
            Assert.NotNull(result.Satellites);
            Assert.Equal(constellation.Satellites.Count, result.Satellites.Count);
        }
    }
}
