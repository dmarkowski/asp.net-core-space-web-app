using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SatelliteTrackerApi.Controllers;
using SatelliteTrackerApi.Models;
using SatelliteTrackerApi.Repository;
using SatelliteTrackerShared.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SatelliteTrackerApiTests.Controllers
{
    public class SatelliteApiControllerTest
    {
        private readonly SatelliteApiController controller;
        private readonly Mock<ISatelliteRepository> satelliteRepository;
        private readonly IFixture fixture;

        public SatelliteApiControllerTest()
        {
            this.satelliteRepository = new Mock<ISatelliteRepository>();
            this.controller = new SatelliteApiController(this.satelliteRepository.Object);
            this.fixture = new Fixture();
            this.fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            this.fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Fact]
        public void Ctor_IfArgumentNull_ShouldThrow()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(() => new SatelliteApiController(null));
        }

        [Fact]
        public async Task Get_IfRepositoryReturnsNull_ShouldReturnNotFound()
        {
            // arrange
            this.satelliteRepository.Setup(_ => _.Get()).ReturnsAsync(null as List<Satellite>);

            // act
            var result = await this.controller.Get();

            // assert
            Assert.NotNull(result?.Result);
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task Get_IfRepositoryReturnsList_ShouldReturnList()
        {
            // arrange
            var entities = this.fixture.CreateMany<Satellite>(3).ToList();
            this.satelliteRepository.Setup(_ => _.Get()).ReturnsAsync(entities);

            // act
            var result = await this.controller.Get();

            // assert
            Assert.NotNull(result?.Result);
            Assert.IsType<OkObjectResult>(result.Result);
            var resultValue = (result.Result as OkObjectResult).Value;
            Assert.NotNull(resultValue);
            Assert.IsType<List<SatelliteDto>>(resultValue);
            Assert.Equal(entities.Count, (resultValue as List<SatelliteDto>).Count);
        }

        [Fact]
        public async Task GetById_IfRepositoryReturnsNull_ShouldReturnNotFound()
        {
            // arrange
            var id = 13;
            this.satelliteRepository.Setup(_ => _.Get(id)).ReturnsAsync(null as Satellite);

            // act
            var result = await this.controller.Get(id);

            // assert
            Assert.NotNull(result?.Result);
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task GetByName_IfRepositoryReturnConstellation_ShouldReturnConstellation()
        {
            // arrange
            var entity = this.fixture.Create<Satellite>();
            var id = 13;
            this.satelliteRepository.Setup(_ => _.Get(id)).ReturnsAsync(entity);

            // act
            var result = await this.controller.Get(id);

            // assert
            Assert.NotNull(result?.Result);
            Assert.IsType<OkObjectResult>(result.Result);
            var resultValue = (result.Result as OkObjectResult).Value;
            Assert.NotNull(resultValue);
            Assert.IsType<SatelliteDto>(resultValue);
        }

        [Fact]
        public async Task Update_ShouldCallSatelliteRepositoryUpdate()
        {
            // arrange
            var satellite = this.fixture.Create<SatelliteDto>();

            // act
            var result = await this.controller.Update(satellite);

            // assert
            Assert.NotNull(result);
            Assert.IsType<OkResult>(result);
            this.satelliteRepository.Verify(_ => _.Update(It.IsAny<Satellite>()), Times.Once);
        }
    }
}
