using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SatelliteTrackerApi.Controllers;
using SatelliteTrackerApi.Models;
using SatelliteTrackerApi.Repository;
using SatelliteTrackerShared.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SatelliteTrackerApiTests.Controllers
{
    public class ConstellationApiControllerTest
    {
        private readonly ConstellationApiController controller;
        private readonly Mock<IConstellationRepository> constellationReposity;
        private readonly Mock<ISatelliteRepository> satelliteRepository;
        private readonly IFixture fixture;

        public ConstellationApiControllerTest()
        {
            this.constellationReposity = new Mock<IConstellationRepository>();
            this.satelliteRepository = new Mock<ISatelliteRepository>();
            this.controller = new ConstellationApiController(this.constellationReposity.Object, this.satelliteRepository.Object);
            this.fixture = new Fixture();
            this.fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            this.fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        public static IEnumerable<object[]> CtorData => new List<object[]>
        {
            new object[] { null, new Mock<ISatelliteRepository>().Object },
            new object[] { new Mock<IConstellationRepository>().Object, null }
        };

        [Theory]
        [MemberData(nameof(CtorData))]
        public void Ctor_IfArgumentNull_ShouldThrow(IConstellationRepository constellationRepository, ISatelliteRepository satelliteRepository)
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(() => new ConstellationApiController(constellationRepository, satelliteRepository));
        }

        [Fact]
        public async Task Get_IfRepositoryReturnsNull_ShouldReturnNotFound()
        {
            // arrange
            this.constellationReposity.Setup(_ => _.Get()).ReturnsAsync(null as List<Constellation>);

            // act
            var result = await this.controller.Get();

            // assert
            Assert.NotNull(result?.Result);
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task Get_IfRepositoryReturnsList_ShouldReturnList()
        {
            // arrange
            var entities = this.fixture.CreateMany<Constellation>(3).ToList();
            this.constellationReposity.Setup(_ => _.Get()).ReturnsAsync(entities);

            // act
            var result = await this.controller.Get();

            // assert
            Assert.NotNull(result?.Result);
            Assert.IsType<OkObjectResult>(result.Result);
            var resultValue = (result.Result as OkObjectResult).Value;
            Assert.NotNull(resultValue);
            Assert.IsType<List<ConstellationBasicDto>>(resultValue);
            Assert.Equal(entities.Count, (resultValue as List<ConstellationBasicDto>).Count);
        }

        [Fact]
        public async Task GetByName_IfRepositoryReturnsNull_ShouldReturnNotFound()
        {
            // arrange
            var name = "collection_name";
            this.constellationReposity.Setup(_ => _.Get(name)).ReturnsAsync(null as Constellation);

            // act
            var result = await this.controller.Get(name);

            // assert
            Assert.NotNull(result?.Result);
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task GetByName_IfRepositoryReturnConstellation_ShouldReturnConstellation()
        {
            // arrange
            var entity = this.fixture.Create<Constellation>();
            var name = "collection_name";
            this.constellationReposity.Setup(_ => _.Get(name)).ReturnsAsync(entity);

            // act
            var result = await this.controller.Get(name);

            // assert
            Assert.NotNull(result?.Result);
            Assert.IsType<OkObjectResult>(result.Result);
            var resultValue = (result.Result as OkObjectResult).Value;
            Assert.NotNull(resultValue);
            Assert.IsType<ConstellationDto>(resultValue);
        }

        [Fact]
        public async Task Update_ShouldCallSatelliteRepositoryUpdateAndConstellationRepositoryUpdate()
        {
            // arrange
            var constellation = this.fixture.Create<ConstellationDto>();

            // act
            var result = await this.controller.Update(constellation);

            // assert
            Assert.NotNull(result);
            Assert.IsType<OkResult>(result);
            this.satelliteRepository.Verify(_ => _.Update(It.IsAny<Satellite>()), Times.Exactly(constellation.Satellites.Count));
            this.constellationReposity.Verify(_ => _.Update(constellation.Name, constellation.Description, constellation.Count, It.Is<IEnumerable<int>>(_ => _.All(constellation.Satellites.Select(_ => _.Id).Contains))), Times.Once);
        }

        [Fact]
        public async Task Update_IfRequestWithoutSatellites_ShouldNotCallSatelliteRepositoryUpdateAndCallConstellationRepositoryUpdate()
        {
            // arrange
            var constellation = this.fixture.Create<ConstellationDto>();
            constellation.Satellites = Array.Empty<SatelliteDto>();

            // act
            var result = await this.controller.Update(constellation);

            // assert
            Assert.NotNull(result);
            Assert.IsType<OkResult>(result);
            this.satelliteRepository.Verify(_ => _.Update(It.IsAny<Satellite>()), Times.Never);
            this.constellationReposity.Verify(_ => _.Update(constellation.Name, constellation.Description, constellation.Count, It.Is<IEnumerable<int>>(_ => !_.Any())), Times.Once);
        }
    }
}
